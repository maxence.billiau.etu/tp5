import Page from './Page';
import PizzaThumbnail from '../components/PizzaThumbnail';
import $ from 'jquery';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList'); // on pase juste la classe CSS souhaitée
		this.pizzas = pizzas;
	}

	mount(element) {
		super.mount(element);
		// appel ajax lorsque l'on affiche la page
		$('.pageContent').addClass('is-loading');
		fetch('http://localhost:8080/api/v1/pizzas')
			.then(response => response.json())
			.then(data => (this.pizzas = data)) //  maj des children
			.then(() => $(this.element).html(this.render())) // affichage dans la page
			.then(() => $('.pageContent').removeClass('is-loading'))
			.then(() => $(() => this.addListeners(element)));
	}

	addListeners(element) {
		$('.pizzaList  img', this.element).on('click', event => {
			event.preventDefault();
			console.log($(event.currentTarget).attr('src'));
		});
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
