<img src="images/readme/header-small.jpg" >

# A. Préparatifs <!-- omit in toc -->

Vous commencez maintenant à avoir l'habitude, je ne rentrerais donc pas dans les détails mais voici les différentes étapes pour le lancement du projet en mode [TL;DR](https://en.wiktionary.org/wiki/tl;dr)

**Attention quand même**, pour ce nouveau TP, on a une petite différence avec les précédents : il ne faut pas oublier de lancer l'API REST (_voir plus bas_)


1. **Créez un fork de ce TP sur https://gitlab.univ-lille.fr/js/tp5/-/forks/new**

	Choisissez de placer le fork dans votre profil utilisateur et configurez le repo **en mode "privé"** (**`Settings`** > **`Visibility, project features, permissions`** > **`Project visibility`**)


2. **Ajoutez-votre encadrant de TP en tant que "reporter" pour qu'il ait accès à votre code :**
	- dans le menu de gauche, cliquez sur **`Project information`** > **`Members`**
	- cliquez sur le bouton en haut à droite **`"Invite members"`**
	- entrez comme **nom d'utilisateur** celui de votre encadrant de TP (`@nicolas.anquetil`, `@patricia.everaere-caillier` ou `@thomas.fritsch`)
	- ... et **`reporter`** comme **rôle**.


3. **Clonez votre fork :**
	```bash
	mkdir ~/tps-js
	git clone https://gitlab.univ-lille.fr/<votre-username>/tp5.git ~/tps-js/tp5
	```

4. **Ouvrez le projet dans VSCodium/VSCode :**
	```bash
	codium ~/tps-js/tp5
	```

5. **Puis dans 3 terminaux splittés de VSCodium/VSCode :**
	```bash
	npm i && npm run watch
	```

	> _**NB :** `npm i ...` est un **raccourci** pour `npm install ...` et l'opérateur `&&` permet de chaîner 2 commandes_

	```bash
	java -jar pizzaland-jar-with-dependencies.jar
	```

	> _**NB :** à lancer dans le dossier où vous aviez téléchargé le serveur REST [lors du précédent TP](https://gitlab.univ-lille.fr/js/tp4/-/blob/master/B-ajax.md#b3-appeler-une-api-restjson-en-get)_

	puis enfin :
	```bash
	npx serve -s -l 8000
	```

	> _**NB :** **si vous souhaitez plus de précisions** sur les commandes précédentes et l'installation  / configuration du projet, vous pouvez vous référer au chapitre [A. Préparatifs](https://gitlab.univ-lille.fr/js/tp4/-/blob/master/A-preparatifs.md) du précédent TP ou simplement demander de l'aide à votre professeur_ 😄

Le résultat attendu est le suivant :

<img src="images/readme/pizzaland-00.png" >

## Étape suivante <!-- omit in toc -->
Si la compilation fonctionne, vous pouvez passer à l'étape suivante : [B. jQuery : les bases](B-jquery-bases.md)